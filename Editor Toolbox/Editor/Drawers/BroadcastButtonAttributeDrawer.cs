﻿using UnityEditor;
using UnityEngine;
using System.Reflection;
namespace Toolbox.Editor
{
    [CustomPropertyDrawer(typeof(BroadcastButtonAttribute))]
    public class BroadcastButtonAttributeDrawer : DecoratorDrawer
    {
        public override float GetHeight()
        {
            return Style.height + Style.spacing;
        }

        public override void OnGUI(Rect position)
        {
            position.height = Style.height;

            var disable = false;
            switch (Attribute.Type)
            {
                case ButtonActivityType.Everything:
                    break;
                case ButtonActivityType.Nothing:
                    disable = true;
                    break;
                case ButtonActivityType.OnEditMode:
                    disable = Application.isPlaying;
                    break;
                case ButtonActivityType.OnPlayMode:
                    disable = !Application.isPlaying;
                    break;
            }

            EditorGUI.BeginDisabledGroup(disable);
            if (GUI.Button(position, string.IsNullOrEmpty(Attribute.Label) ? Attribute.MethodName : Attribute.Label))
            {
                foreach (var target in Selection.gameObjects)
                {
                    if (target == null) return;
                    //Object obj = target.GetComponent<Object>() as Object;
                    //MethodInfo tMethod = target.GetType().GetMethod(Attribute.MethodName);
                    //if(tMethod != null)
                    //{
                    //    Debug.Log("Broadcast:" + tMethod.Name + "\n" +
                    //        "DeclaringType:" + tMethod.DeclaringType.ToString() +
                    //        "ReflectedType:" + tMethod.ReflectedType.ToString() +
                    //        "MethodImplementationFlags:" + tMethod.MethodImplementationFlags.ToString() +
                    //        "CustomAttributes:" + tMethod.CustomAttributes.ToString() +
                    //        "IsPrivate:" + tMethod.IsPrivate.ToString() +
                    //        "IsPublic:" + tMethod.IsPublic.ToString() +
                    //        "IsVirtual:" + tMethod.IsVirtual.ToString() +
                    //        "IsStatic:" + tMethod.IsStatic.ToString() +
                    //        "IsFamily:" + tMethod.IsFamily.ToString() +
                    //        "Attributes:" + tMethod.Attributes.ToString()
                    //        );
                    //    tMethod.Invoke(obj,tMethod.GetGenericArguments());
                        
                    //}
                    target.SendMessage(Attribute.MethodName, attribute.order,SendMessageOptions.RequireReceiver);
                }
            }
            EditorGUI.EndDisabledGroup();
        }


        /// <summary>
        /// A wrapper which returns the PropertyDrawer.attribute field as a <see cref="BroadcastButtonAttribute"/>.
        /// </summary>
        private BroadcastButtonAttribute Attribute => attribute as BroadcastButtonAttribute;


        /// <summary>
        /// Static representation of button style.
        /// </summary>
        private static class Style
        {
            internal static readonly float height = EditorGUIUtility.singleLineHeight * 1.25f;
            internal static readonly float spacing = EditorGUIUtility.standardVerticalSpacing;

            internal static GUIStyle buttonStyle;

            static Style()
            {
                buttonStyle = new GUIStyle(GUI.skin.button);
            }
        }
    }
}